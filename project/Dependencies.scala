import sbt._

object Version {
  lazy val akkaHttpVersion = "10.2.1"
  lazy val akkaVersion     = "2.6.10"
  lazy val playJson        = "2.9.1"
}

object Library {
  val akkahttp            = "com.typesafe.akka"           %% "akka-http"                   % Version.akkaHttpVersion
  val akkastream          = "com.typesafe.akka"           %% "akka-stream"                 % Version.akkaVersion
  val playjson            = "com.typesafe.play"           %% "play-json"                   % Version.playJson
  val logback             = "ch.qos.logback"              %  "logback-classic"             % "1.2.3"
  val akkahttptestkit     = "com.typesafe.akka"           %% "akka-http-testkit"           % Version.akkaHttpVersion
  val akkatestkit         = "com.typesafe.akka"           %% "akka-testkit"                % Version.akkaVersion
  val akkastreamtestkit   = "com.typesafe.akka"           %% "akka-stream-testkit"         % Version.akkaVersion
  val scalatest           = "org.scalatest"               %% "scalatest"                   % "3.2.3"
  val mockito             = "org.mockito"                 %% "mockito-scala"               % "1.16.3"
  val anorm               = "org.playframework.anorm"     %% "anorm"                       % "2.6.8"
  val postgresql          = "org.postgresql"              %  "postgresql"                  % "9.4.1212"
  val hikaricp            = "com.zaxxer"                  %  "HikariCP"                    % "3.4.5"
  val h2                  = "com.h2database"              % "h2"                           % "1.4.200"

}

object Dependencies {

  import Library._

  val scalaVersion = "2.13.4"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us"),
    ("Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/")
  )

  val runDep = run(
    akkahttp,
    akkastream,
    playjson,
    logback,
    anorm,
    postgresql,
    hikaricp
  )

  val testDep = test(
    akkahttptestkit,
    akkatestkit,
    akkastreamtestkit,
    scalatest,
    mockito,
    hikaricp,
    h2
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % Test)

}
