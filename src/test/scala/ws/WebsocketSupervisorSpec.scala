package ws

import akka.actor.{Actor, ActorSystem, Props}
import akka.pattern.BackoffSupervisor
import akka.testkit.{ImplicitSender, TestKit}
import app.Configuration
import org.mockito.MockitoSugar
import org.scalatest._
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent.duration._

class WebsocketSupervisorSpec(_system: ActorSystem) extends TestKit(_system)
    with ImplicitSender with AnyWordSpecLike with Matchers with BeforeAndAfterAll with MockitoSugar with Eventually {

  class Helper {
    val config: Configuration = mock[Configuration]

    when(config.restartRetryMin).thenReturn(1.seconds)
    when(config.restartRetryMax).thenReturn(1.seconds)

    case object CrashItself
    class FailingActor extends Actor {
      override def receive = {
        case CrashItself => throw new Exception("Crashing itself")
      }
    }

    val wsSupervisor = system.actorOf(WebsocketSupervisor.initWebsocketActor(config, Props(new FailingActor)), name = "wsSupervisor")
  }

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
    _system.terminate()
  }

  "WebsocketSupervisor" should {
    "restart websocket actor" in new Helper {
      wsSupervisor ! BackoffSupervisor.GetCurrentChild
      val Some(child) = expectMsgType[BackoffSupervisor.CurrentChild].ref

      child ! CrashItself

      eventually(timeout(scaled(2500.milliseconds))) {
        wsSupervisor ! BackoffSupervisor.GetCurrentChild
        expectMsgType[BackoffSupervisor.CurrentChild].ref.get should !==(child)

        wsSupervisor ! BackoffSupervisor.GetRestartCount
        expectMsg(BackoffSupervisor.RestartCount(1))
      }

    }
  }
}
