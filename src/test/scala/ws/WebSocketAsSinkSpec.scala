package ws

import akka.Done
import akka.actor.SupervisorStrategy.Stop
import akka.actor.{Actor, ActorSystem, OneForOneStrategy, SupervisorStrategy}
import akka.http.scaladsl.model.ws.{Message, TextMessage, ValidUpgrade, WebSocketUpgradeResponse}
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.stream.scaladsl.{Flow, Source}
import akka.testkit.TestActorRef
import app.Configuration
import models.events.ws.{StasisEnd, StasisStart}
import models.{Caller, Channel, Connected, Dialplan}
import org.mockito.{ArgumentMatchersSugar, MockitoSugar}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus
import services.AriEventBus._

import scala.concurrent.{Future, Promise}

class DummySupervisor extends Actor {
  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case e: Exception => Stop
  }

  override def receive: Receive = {
    case _ =>
  }
}

class WebSocketAsSinkSpec extends AnyWordSpecLike with Matchers with MockitoSugar with ArgumentMatchersSugar {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    implicit val ec = system.dispatcher

    val config: Configuration = mock[Configuration]
    val ariBus: AriEventBus = mock[AriEventBus]

    def getStasisStartEvent(app: String = "outcalls") = s"""{"type":"StasisStart","timestamp":"2019-02-05T14:07:09.004+0100","args":[],"channel":{"id":"1549372028.17","name":"SIP/mnwggob4-00000011","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-05T14:07:08.895+0100","language":"en_US"},"asterisk_id":"08:00:27:80:c1:5f","application":"$app"}"""

    val channelHangupRequestEvent = """{"type":"ChannelHangupRequest","timestamp":"2019-02-06T09:49:38.674+0100","channel":{"id":"1549442948.0","name":"SIP/mnwggob4-00000000","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-06T09:49:08.709+0100","language":"en_US"},"asterisk_id":"08:00:27:80:c1:5f","application":"outcalls"}"""
    val stasisEndEvent = """{"type":"StasisEnd","timestamp":"2019-02-06T09:49:38.675+0100","channel":{"id":"1549372028.17","name":"SIP/mnwggob4-00000011","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-05T14:07:08.895+0100","language":"en_US"},"asterisk_id":"08:00:27:80:c1:5f","application":"outcalls"}"""
    val unsupportedEvent = """{"type":"UnsupportedEvent","timestamp":"2019-02-05T14:07:09.004+0100","args":[],"channel":{"id":"1549372028.17","name":"SIP/mnwggob4-00000011","state":"Up","caller":{"name":"User One","number":"1001"},"connected":{"name":"","number":""},"accountcode":"","dialplan":{"context":"default","exten":"9999","priority":3},"creationtime":"2019-02-05T14:07:08.895+0100","language":"en_US"},"asterisk_id":"08:00:27:80:c1:5f","application":"outcalls"}"""

    val caller = Caller(Some("User One"), Some("1001"))
    val connected = Connected(Some(""), Some(""))
    val dialplan = Dialplan("default", "9999", 3)
    val channel = Channel("1549372028.17", "SIP/mnwggob4-00000011", "Up", caller, connected, Some(""), dialplan, "2019-02-05T14:07:08.895+0100", "en_US")

    val upgradeMessage = ValidUpgrade(HttpResponse(StatusCodes.SwitchingProtocols), None)
    val upgradeResponseFuture = Future.successful(upgradeMessage)
    val done: Future[Done] = Future(mock[Done])
    val promise: Promise[Option[Message]] = Promise()

    class TestWebsocketRequestFactory extends WebsocketRequestFactory {
      def getWsRequest(appName: OutcallApp, wsFlow: Flow[Message, Message, (Future[Done], Promise[Option[Message]])]): (Future[WebSocketUpgradeResponse], (Future[Done], Promise[Option[Message]])) = {
        (upgradeResponseFuture, (done, promise))
      }
    }

    val dummySupervisor = TestActorRef[DummySupervisor]

    val websocketActor: TestActorRef[WebsocketAsSink] = TestActorRef[WebsocketAsSink](
      WebsocketAsSink.websocketProps(config, ariBus, new TestWebsocketRequestFactory()),
      dummySupervisor, "websocketTestActor"
    )
    val ws: WebsocketAsSink = websocketActor.underlyingActor
  }

  "WebSocketAsSink" should {
    "receive from websocket and publish bus event for schedule application" in new Helper {
      val source = Source.single(TextMessage.Strict(getStasisStartEvent("schedules")))
      val event = StasisStart(List(), channel, "08:00:27:80:c1:5f", "schedules")

      val expected = AriEvent(AriTopic(TopicType.WSEVENT, ScheduleApp), AriWsMessage(event))
      ws.incomingMessage.runWith(source)

      verify(ariBus, timeout(2000)).publish(expected)
    }

    "receive from websocket and publish bus event for rights application" in new Helper {
      val source = Source.single(TextMessage.Strict(getStasisStartEvent("rights")))
      val event = StasisStart(List(), channel, "08:00:27:80:c1:5f", "rights")

      val expected = AriEvent(AriTopic(TopicType.WSEVENT, RightsApp), AriWsMessage(event))
      ws.incomingMessage.runWith(source)

      verify(ariBus, timeout(2000)).publish(expected)
    }

    "receive and not publish unsupported ws event" in new Helper {
      val source = Source.single(TextMessage.Strict(unsupportedEvent))
      ws.incomingMessage.runWith(source)

      verifyZeroInteractions(ariBus)
    }

    "process received ws event StasisStart" in new Helper {
      val message = TextMessage.Strict(getStasisStartEvent())
      ws.processEvents(message) shouldBe Some(StasisStart(List(), channel, "08:00:27:80:c1:5f", "outcalls"))
    }

    "process received ws event StasisEnd" in new Helper {
      val message = TextMessage.Strict(stasisEndEvent)
      ws.processEvents(message) shouldBe Some(StasisEnd(channel, "08:00:27:80:c1:5f", "outcalls"))
    }

    "process received ws event Unsupported" in new Helper {
      val message = TextMessage.Strict(channelHangupRequestEvent)
      ws.processEvents(message) shouldBe None
    }

    "process received ws event Error" in new Helper {
      val errorMessage = """{"some":"thing"}"""
      val message = TextMessage.Strict(errorMessage)
      val thrown = intercept[Exception] {
        ws.processEvents(message)
      }
      thrown.getMessage shouldBe "Non understandable ws event received"
    }
  }
}
