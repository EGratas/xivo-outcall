package ws

import akka.actor.ActorSystem
import app.Configuration
import org.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus.{RightsApp, ScheduleApp}

class WebsocketRequestFactorySpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  class Helper {
    implicit val system: ActorSystem = ActorSystem()

    val config = mock[Configuration]

    when(config.ariHost).thenReturn("localhost")
    when(config.ariPort).thenReturn("8080")
    when(config.ariUsername).thenReturn("username")
    when(config.ariPassword).thenReturn("password")

    val factory = new ProductionWebsocketRequestFactory(config)
  }

  "WebsocketRequestFactory" should {
    "create websocket uri" in new Helper {
      factory.createWsUri(ScheduleApp.toString) shouldBe "ws://localhost:8080/ari/events?api_key=username:password&app=schedules"
      factory.createWsUri(RightsApp.toString) shouldBe "ws://localhost:8080/ari/events?api_key=username:password&app=rights"
    }
  }
}
