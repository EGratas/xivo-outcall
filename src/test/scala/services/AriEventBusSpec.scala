package services

import akka.actor.ActorSystem
import akka.testkit.TestProbe
import models.events.ws.StasisStart
import org.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import services.AriEventBus._

class AriEventBusSpec extends AnyWordSpecLike with Matchers with MockitoSugar {

  class Helper() {
    implicit val system: ActorSystem = ActorSystem()
    val scheduleFsmManager = TestProbe()
    val ariBus = new AriEventBus()
    val otherFsmManager = TestProbe()
    val fsm = TestProbe()

    val wsEvent = mock[StasisStart]

    case object OtherApp extends OutcallApp
  }

  "AriEventBus" should {
    "subscribe requester on topic" in new Helper {
      val message = AriWsMessage(wsEvent)

      val topicForSchedule = AriTopic(topicType = TopicType.RESTREQUEST, appName = DefaultApp)
      val topicForOtherApp = AriTopic(topicType = TopicType.RESTREQUEST, appName = OtherApp)

      ariBus.subscribe(scheduleFsmManager.ref, topicForSchedule)
      ariBus.subscribe(otherFsmManager.ref, topicForOtherApp)

      ariBus.publish(AriEvent(topicForSchedule, message))

      scheduleFsmManager.expectMsg(AriWsMessage(wsEvent))
      otherFsmManager.expectNoMessage()
    }
  }
}
