package models

import models.ChannelVariables._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class ChannelSpec extends AnyWordSpecLike with Matchers {
  class Helper() {
    val channelData = ChannelData(Map())
  }

  "ChannelData" should {
    "add channel var" in new Helper {
      val expected = ChannelData(Map("XIVO_ROUTE_ID" -> Some("1")))

      channelData.addChannelVar(XIVO_ROUTE_ID, Some("1")) shouldEqual expected
    }

    "get channel var" in new Helper {
      val newChannelData = channelData.addChannelVar("XIVO_USERID", Some("1"))

      newChannelData.getChannelVar(USERID) shouldEqual Some("1")
    }

    "get None for empty channel var" in new Helper {
      val newChannelData = channelData.addChannelVar("XIVO_USERID", Some(""))

      newChannelData.getChannelVar(USERID) shouldEqual None
    }

    "remove one waiting channel var" in new Helper {
      val expected = List("XIVO_USER_ID")

      channelData.removeOneAwaited("XIVO_ROUTE_ID", List("XIVO_ROUTE_ID", "XIVO_USER_ID")) shouldEqual expected
    }
  }

}