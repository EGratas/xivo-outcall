package services.requests

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.model.{ HttpMethod, HttpRequest, HttpResponse }

import scala.concurrent.Future

class AriRest(implicit system: ActorSystem) {

  def singleRequest(uri: String, method: HttpMethod, authHeaders: Authorization): Future[HttpResponse] = {
    Http().singleRequest(
      HttpRequest(uri = uri)
        .withMethod(method)
        .withHeaders(authHeaders)
    )
  }
}
