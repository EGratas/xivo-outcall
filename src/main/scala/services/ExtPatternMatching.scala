package services

import scala.collection.SortedMap

object ExtPatternMatching {
  private final val extensionRegexpMap: SortedMap[String, String] = SortedMap(
    "_" -> "",
    "!" -> "[0-9#\\*]*",
    "X" -> "[0-9]",
    "Z" -> "[1-9]",
    "N" -> "[2-9]",
    "." -> "[0-9#\\*]+",
    "+" -> "\\+"
  )

  private def convert(pattern: String): String = {
    extensionRegexpMap.foldLeft(pattern) {
      case (p, kv) => p.replace(kv._1, kv._2)
    }
  }

  def isMatch(pattern: String, destination: String): Boolean = {
    destination.matches(convert(pattern))
  }
}
