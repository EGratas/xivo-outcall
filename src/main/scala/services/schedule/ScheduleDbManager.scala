package services.schedule

import anorm.SqlParser._
import anorm._
import app.DatabaseConnectionPool
import services.schedule.ScheduleCalendar._
import services.schedule.ScheduleDbManager._
import services.schedule.ScheduleService.RoutePath

import scala.concurrent.Future

object ScheduleDbManager {

  case class SchedulePath(id: Int, timezone: String, fallback_action: ScheduleAction, scheduleName: String)

  case class UncheckedSchedulePeriods(mode: PeriodMode, hours: String, weekdays: Option[String], monthdays: Option[String],
                                      months: Option[String], action: ScheduleAction)

  sealed trait PeriodMode

  case object ModeOpened extends PeriodMode

  case object ModeClosed extends PeriodMode

  implicit def toScheduleAction: (Option[String], Option[String], Option[String]) => ScheduleAction = {
    case (Some(action), actionId, actionArgs) =>
      action match {
        case a if a == "endcall:hangup" => EndcallHangup(a, actionId, actionArgs)
        case a if a == "none" => ProcessInDialplan(a, actionId, actionArgs)
        case other => ProcessInDialplan(other, actionId, actionArgs)
      }
    case (_, _, _) => ScheduleActionNone
  }

  implicit def toPeriodMode: String => PeriodMode = {
    case "opened" => ModeOpened
    case "closed" => ModeClosed
    case _ => ModeOpened
  }

  implicit class toAction(action: (Option[String], Option[String], Option[String])) {
    def asScheduleAction(implicit toScheduleAction: (Option[String], Option[String], Option[String]) => ScheduleAction): ScheduleAction = toScheduleAction(action._1, action._2, action._3)
  }

  implicit class toPeriodMode(mode: String) {
    def asPeriodMode(implicit toPeriodMode: String => PeriodMode): PeriodMode = toPeriodMode(mode)
  }

  def createActionType(action: Option[String], actionId: Option[String], actionArgs: Option[String]): ScheduleAction = {
    (action, actionId, actionArgs).asScheduleAction
  }
}

class ScheduleDbManager(db: DatabaseConnectionPool) {

  def getSchedulePath(routePath: RoutePath): Future[Option[SchedulePath]] = db.withConnection { implicit c =>
    val simple: RowParser[SchedulePath] =
      get[Int]("id") ~
        get[String]("timezone") ~
        get[Option[String]]("fallback_action") ~
        get[Option[String]]("fallback_actionid") ~
        get[Option[String]]("fallback_actionargs") ~
        get[String]("scheduleName") map {
        case id ~ timezone ~ fallbackAction ~ fallbackActionId ~ fallbackActionArgs ~ scheduleName =>
          SchedulePath(id, timezone, ScheduleDbManager.createActionType(fallbackAction, fallbackActionId, fallbackActionArgs), scheduleName)
      }

    SQL(
      """
           SELECT
                       s.id, timezone, fallback_action, fallback_actionid, fallback_actionargs, s.name AS scheduleName
           FROM
                       schedule_path p
           LEFT JOIN
                       schedule s on p.schedule_id = s.id
           WHERE
                       p.path = 'route'
                AND
                       p.pathid = {pathid}
                AND
                       s.commented = 0
        """.stripMargin
    )
      .on(Symbol("path") -> routePath.path, Symbol("pathid") -> routePath.pathId)
      .as(simple.*).headOption
  }

  def getSchedulePeriods(schedulePath: SchedulePath): Future[List[UncheckedSchedulePeriods]] = db.withConnection { implicit c =>
    val simple: RowParser[UncheckedSchedulePeriods] =
      get[String]("mode") ~
        get[String]("hours") ~
        get[Option[String]]("weekdays") ~
        get[Option[String]]("monthdays") ~
        get[Option[String]]("months") ~
        get[Option[String]]("action") ~
        get[Option[String]]("actionid") ~
        get[Option[String]]("actionargs") map {
        case mode ~ hours ~ weekdays ~ monthdays ~ months ~ action ~ actionid ~ actionargs =>
          UncheckedSchedulePeriods(mode.asPeriodMode, hours, weekdays, monthdays, months, ScheduleDbManager.createActionType(action, actionid, actionargs))
      }

    SQL(
      """
           SELECT
                       mode, hours, weekdays, monthdays, months, action, actionid, actionargs
           FROM
                       schedule_time
           WHERE
                       schedule_id = {scheduleId}
        """.stripMargin
    )
      .on(Symbol("scheduleId") -> schedulePath.id)
      .as(simple.*)
  }
}
