package services.schedule

import app.helpers.LogWithChannel
import ari.actions.Ari
import ari.actions.Channels.HangupReason
import models.Channel
import services.schedule.ScheduleCalendar.{ ScheduleActionNone, _ }

class ScheduleActionExecutor(ari: Ari) extends LogWithChannel {

  def processSchedulePeriod(schedule: Schedule, channel: Channel): Unit = {
    schedule match {
      case Schedule(ScheduleStateOpened, _, scheduleName) =>
        chanLog.info(channel, s"schedule ${scheduleName.getOrElse(None)} is currently $ScheduleStateOpened")
        processAction(ScheduleActionNone, channel)
      case Schedule(ScheduleStateClosed, action, scheduleName) =>
        chanLog.info(channel, s"schedule ${scheduleName.getOrElse(None)} is currently $ScheduleStateClosed")
        processAction(action, channel)
    }
  }

  private def processAction(action: ScheduleAction, channel: Channel): Unit = {
    action match {
      case ScheduleActionNone =>
        chanLog.info(channel, "executing no action")
        setOpenedScheduleChannelVars(channel.id)
        ari.channels.continue(channel.id, None, None, None, None, channel.id)
      case a: EndcallHangup =>
        chanLog.info(channel, s"executing action ${a.action} - ${a.actionId} - ${a.actionArgs}")
        ari.channels.hangup(channel.id, Some(HangupReason.busy), channel.id)
      case a: ProcessInDialplan =>
        chanLog.info(channel, s"executing action ${a.action} - ${a.actionId} - ${a.actionArgs}")
        setClosedScheduleChannelVars(channel.id, a)
        ari.channels.continue(channel.id, None, None, None, None, channel.id)
    }
  }

  private def setClosedScheduleChannelVars(channelId: String, scheduleAction: ProcessInDialplan): Unit = {
    ari.channels.setChannelVar(channelId, "XIVO_SCHEDULE_STATUS", Some("closed"), channelId)
    ari.channels.setChannelVar(channelId, "XIVO_PATH", Some(""), channelId)
    ari.channels.setChannelVar(channelId, "XIVO_FWD_SCHEDULE_OUT_ACTION", Some(scheduleAction.action), channelId)
    ari.channels.setChannelVar(channelId, "XIVO_FWD_SCHEDULE_OUT_ACTIONARG1", scheduleAction.actionId, channelId)
    scheduleAction.actionArgs.foreach(actionArgs => ari.channels.setChannelVar(channelId, "XIVO_FWD_SCHEDULE_OUT_ACTIONARG2", Some(actionArgs), channelId))
  }

  private def setOpenedScheduleChannelVars(channelId: String): Unit = {
    ari.channels.setChannelVar(channelId, "XIVO_SCHEDULE_STATUS", None, channelId)
  }
}
