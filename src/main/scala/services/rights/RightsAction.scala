package services.rights

import app.Configuration.ServiceId
import app.helpers.LogWithChannel
import ari.actions.Ari
import models.ChannelVariables._
import models.{ CallAuthorization, CallPassword, CallSettings, Channel }
import services.AriEventBus
import services.AriEventBus.RightsApp
import services.rights.RightsDbActions.RightCall

class RightsAction(ariEventBus: AriEventBus, fsmId: ServiceId) extends LogWithChannel {

  val ari = new Ari(ariEventBus, RightsApp)

  def allow(channel: Channel): List[CallSettings] = {
    chanLog.info(channel, s"allowed to make call")
    ari.channels.setChannelVar(channel.id, XIVO_AUTHORIZATION, Some(ALLOW), fsmId)
    List(CallAuthorization(XIVO_AUTHORIZATION, ALLOW))
  }

  def disallow(channel: Channel, password: Option[String]): List[CallSettings] = {
    chanLog.info(channel, s"not allowed to make call")
    val callSettings: List[CallSettings] = password match {
      case Some(p) =>
        chanLog.info(channel, s"use password to allow")
        ari.channels.setChannelVar(channel.id, XIVO_PASSWORD, Some(p), fsmId)
        List(CallPassword(XIVO_PASSWORD, p), CallAuthorization(XIVO_AUTHORIZATION, DENY))
      case None =>
        List(CallAuthorization(XIVO_AUTHORIZATION, DENY))
    }
    ari.channels.setChannelVar(channel.id, XIVO_AUTHORIZATION, Some(DENY), fsmId)
    callSettings
  }

  def applyRules(channel: Channel, rules: List[RightCall]): List[CallSettings] = {
    chanLog.debug(channel, s"applying call right rules")
    if (rules.isEmpty)
      allow(channel)
    else {
      if (rules.exists(r => r.authorization == 1))
        allow(channel)
      else if (rules.exists(r => r.authorization == 0))
        rules.find(r => r.authorization == 0 && r.password.nonEmpty).map(r => disallow(channel, Some(r.password)))
          .orElse(rules.find(r => r.authorization == 0 && r.password.isEmpty).map(_ => disallow(channel, None)))
          .getOrElse(disallow(channel, None))
      else
        disallow(channel, None)
    }
  }

  def continueInDialplan(channel: Channel): Unit = {
    chanLog.info(channel, "continue in dialplan")
    ari.channels.continue(channel.id, None, None, None, None, fsmId)
  }
}
