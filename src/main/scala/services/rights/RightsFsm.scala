package services.rights

import ari.actions.Ari
import models.ChannelVariables._
import models._
import models.events.ws.{ChannelVarset, StasisStart}
import services.FsmFactory.{Initial, Uninitialized}
import services._
import services.rights.RightsDbActions.{RightCall, User}
import services.rights.RightsDbManager._
import services.rights.RightsFsm._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object RightsFsm {
  case object AwaitChannelVars extends FsmState
  case object AwaitSetRights extends FsmState
  case object UserHasRight extends FsmState
  case object GroupOrRouteHasRight extends FsmState
  case object AwaitRightsForRoute extends FsmState
  case object AwaitRightsIds extends FsmState
  case object AwaitUserId extends FsmState

  case class RightsFsmContext(channelData: ChannelData, awaiting: List[String], forwardType: Option[ForwardType] = None,
    channel: Channel) extends Context
  case class RightsFsmContextWitUserId(contextData: ChannelData, id: Option[String], forwardType: Option[ForwardType] = None, channel: Channel)

  case class RichRightsFsmContext(rightCallsIds: List[RightsDbActions.RightCallExten], dstNum: Option[String],
    validatedRightCallsIds: List[RightsDbActions.RightCallExten], user: Option[User], awaiting: List[String], channelData: ChannelData,
    channel: Channel) extends Context
}

class RightsFsm(ariEventBus: AriEventBus, ariImp: Ari, databaseManagers: DatabaseManagers, rightsHelper: RightsHelper,
    rightsRepository: RightsFsmRepository, channelImpl: Channel) extends FsmFactory {

  implicit val ec: ExecutionContext = context.dispatcher

  val rightsAction = new RightsAction(ariEventBus, channelImpl.id)

  override val ari: Ari = ariImp
  override val channel: Channel = channelImpl
  override val dbManagers: DatabaseManagers = databaseManagers

  startWith(Initial, Uninitialized)

  when(Initial) {
    case Event(e: StasisStart, Uninitialized) =>
      chanLog.info(e.channel, "entering rights checks")
      ari.channels.getChannelVar(e.channel.id, USERID, channel.id)
      goto(AwaitUserId) using RightsFsmContext(ChannelData(Map()), List(), None, e.channel)
  }

  when(AwaitUserId, stateTimeout = 3.second) {
    case Event(e: ChannelVarResponse, fsmContext: RightsFsmContext) =>
      val ctxData = ChannelData(Map(e.channelVarName -> e.channelVarValue.`value`))
      val waitChannelVars = rightsRepository.getRequiredChannelVars
      waitChannelVars.foreach(v => ari.channels.getChannelVar(fsmContext.channel.id, v, channel.id))
      goto(AwaitChannelVars) using RightsFsmContext(ctxData, waitChannelVars, None, fsmContext.channel)
  }

  when(AwaitChannelVars, stateTimeout = 3.second) {
    case Event(e: ChannelVarResponse, fsmContext: RightsFsmContext) =>
      val newCtxData = fsmContext.channelData.addChannelVar(e.channelVarName, e.channelVarValue.`value`)
      val expectedChVars = newCtxData.removeOneAwaited(e.channelVarName, fsmContext.awaiting)
      if (expectedChVars.nonEmpty)
        stay() using RightsFsmContext(newCtxData, expectedChVars, None, fsmContext.channel)
      else {
        if (newCtxData.getChannelVar(USERID).isEmpty && newCtxData.getChannelVar(XIVO_ROUTE_ID).isEmpty) {
          allow(fsmContext.channel, fsmContext.channelData)
        } else {
          decideForwardState(RightsFsmContext(newCtxData, expectedChVars, None, fsmContext.channel))
        }
      }
  }

  when(AwaitRightsIds, stateTimeout = 3.second) {
    case Event(r: RichRightsFsmContext, fsmContext: RightsFsmContext) =>
      if (r.validatedRightCallsIds.isEmpty) {
        allow(fsmContext.channel, fsmContext.channelData)
      } else {
        if (r.user.isEmpty) {
          if (r.channelData.getChannelVar(XIVO_ROUTE_ID).isEmpty) {
            allow(r.channel, r.channelData)
          } else {
            databaseManagers.rightsManager ! GetRightCallRoute(r.channelData.getChannelVar(XIVO_ROUTE_ID).get, r.validatedRightCallsIds)
            goto(AwaitRightsForRoute) using r
          }
        } else {
          databaseManagers.rightsManager ! GetUser(r.user.get.id)
          stay() using r
        }
      }

    case Event(response: ResponseUser, fsmContext: RichRightsFsmContext) =>
      response.user match {
        case Some(u) =>
          chanLog.info(fsmContext.channel, s"checking right of user (userid:${u.id}) to call number ${fsmContext.dstNum}")
          val newCtx = RichRightsFsmContext(fsmContext.rightCallsIds, fsmContext.dstNum, fsmContext.validatedRightCallsIds, Some(u), List(), fsmContext.channelData, fsmContext.channel)
          fsmContext.dstNum match {
            case None =>
              allow(fsmContext.channel, fsmContext.channelData)
            case Some(dstNum) =>
              databaseManagers.rightsManager ! GetRightCallUser(u.id.toString, newCtx.validatedRightCallsIds)
              goto(UserHasRight) using newCtx
          }
        case None =>
          databaseManagers.rightsManager ! GetRightCallRoute(fsmContext.channelData.getChannelVar(XIVO_ROUTE_ID).get, fsmContext.validatedRightCallsIds)
          goto(AwaitRightsForRoute) using fsmContext
      }
  }

  when(UserHasRight, stateTimeout = 3.second) {
    case Event(response: ResponseRightCalls, fsmContext: RichRightsFsmContext) =>
      response.rights match {
        case RightsFound(r) =>
          val userRules: List[RightCall] = fsmContext.user.get.rightcallcode
            .map(code => r.map(r => r.copy(password = code)))
            .getOrElse(r)
          applyRules(fsmContext.channel, fsmContext.channelData, userRules)
        case RightsNotFound =>
          databaseManagers.rightsManager ! GetGroupRights(fsmContext.user.get.id, fsmContext.validatedRightCallsIds)
          goto(GroupOrRouteHasRight) using fsmContext
        case RightsFailed(f) => errorHandling(s"Failed to get user rights, $f")
      }
  }

  when(GroupOrRouteHasRight, stateTimeout = 3.seconds) {
    case Event(response: ResponseRightCalls, fsmContext: RichRightsFsmContext) =>
      response.rights match {
        case RightsFound(r) => applyRules(fsmContext.channel, fsmContext.channelData, r)
        case RightsNotFound =>
          databaseManagers.rightsManager ! GetRightCallRoute(fsmContext.channelData.getChannelVar(XIVO_ROUTE_ID).get, fsmContext.validatedRightCallsIds)
          goto(AwaitRightsForRoute) using fsmContext
        case RightsFailed(f) =>
          chanLog.error(channel, s"Failed to get group rights, $f")
          databaseManagers.rightsManager ! GetRightCallRoute(fsmContext.channelData.getChannelVar(XIVO_ROUTE_ID).get, fsmContext.validatedRightCallsIds)
          goto(AwaitRightsForRoute) using fsmContext

      }
  }

  when(AwaitRightsForRoute, stateTimeout = 3.seconds) {
    case Event(response: ResponseRightCalls, fsmContext: RichRightsFsmContext) =>
      response.rights match {
        case RightsFound(r) => applyRules(fsmContext.channel, fsmContext.channelData, r)
        case RightsNotFound => applyRules(fsmContext.channel, fsmContext.channelData, List())
        case RightsFailed(f) => errorHandling(s"Failed to get route rights, $f")
      }
  }

  when(AwaitSetRights, stateTimeout = 3.second) {
    case Event(e: ChannelVarset, fsmContext: RightsFsmContext) =>
      val awaitingChannelVars = fsmContext.channelData.removeOneAwaited(e.`variable`, fsmContext.awaiting)
      if (awaitingChannelVars.nonEmpty)
        stay() using RightsFsmContext(fsmContext.channelData, fsmContext.awaiting.filterNot(_ == e.`variable`), None, fsmContext.channel)
      else {
        rightsAction.continueInDialplan(e.channel)
        stop()
      }
  }

  private def decideForwardState(fsmContext: RightsFsmContext): State = {
    rightsHelper.isCallForwarded(fsmContext.channelData.vars) match {
      case Some(fwdType) =>
        getForwarderUserIdIfExists(fwdType, fsmContext, rightsHelper.getIdFromChannel).map {
          case ForwardedUser(id) => databaseManagers.rightsManager ! GetRightsIds(RightsFsmContextWitUserId(fsmContext.channelData, Some(id), Some(fwdType), fsmContext.channel))
          case ForwardedElseWhere => databaseManagers.rightsManager ! GetRightsIds(RightsFsmContextWitUserId(fsmContext.channelData, None, None, fsmContext.channel))
        }
        goto(AwaitRightsIds)
      case None =>
        databaseManagers.rightsManager ! GetRightsIds(RightsFsmContextWitUserId(fsmContext.channelData, None, None, fsmContext.channel))
        goto(AwaitRightsIds) using fsmContext
    }
  }

  private def allow(channel: Channel, contextData: ChannelData) = {
    val setChannelVars = rightsAction.allow(channel).map(_.variableName)
    goto(AwaitSetRights) using RightsFsmContext(contextData, setChannelVars, None, channel)
  }

  private def applyRules(channel: Channel, contextData: ChannelData, rules: List[RightCall]) = {
    val setChannelVars = rightsAction.applyRules(channel, rules).map(_.variableName)
    goto(AwaitSetRights) using RightsFsmContext(contextData, setChannelVars, None, channel)
  }
}
