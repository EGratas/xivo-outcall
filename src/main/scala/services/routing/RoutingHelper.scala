package services.routing

import ari.actions.Ari
import models.ChannelVariables._
import models.events.ws.ChannelVarset
import models.{ Channel, ChannelData, ChannelVarResponse }
import services.AriEventBus.RoutingApp
import services.routing.RoutingDbActions._
import services.routing.RoutingFsm._
import services.routing.RoutingHelper.RouteInputData
import services.{ AriEventBus, Context }

object RoutingHelper {
  case class RouteInputData(mdsName: String, destination: String, context: String)
}

class RoutingHelper(ariEventBus: AriEventBus) {

  val ari = new Ari(ariEventBus, RoutingApp)

  def updateChannelData(e: ChannelVarResponse, channelData: ChannelData): ChannelData = {
    channelData.addChannelVar(e.channelVarName, e.channelVarValue.`value`)
  }

  def removeOneAwaited(e: ChannelVarResponse, context: Context): List[String] = {
    context.channelData.removeOneAwaited(e.channelVarName, context.awaiting)
  }

  def removeOneAwaited(e: ChannelVarset, context: Context): List[String] = {
    context.channelData.removeOneAwaited(e.`variable`, context.awaiting)
  }

  def getDataForRoute(channelData: ChannelData): Option[RouteInputData] = for {
    mdsName <- channelData.getChannelVar(MDS_NAME)
    destination <- channelData.getChannelVar(DESTINATION_NUMBER)
    context <- channelData.getChannelVar(XIVO_BASE_CONTEXT)
  } yield RouteInputData(mdsName, destination, context)

  def setChannelVars(channel: Channel, varName: String, varValue: Option[String]): Unit = {
    ari.channels.setChannelVar(channel.id, varName, varValue, channel.id)
  }

  def regexTransformation(route: Route, destination: String): String = {
    (route.regexp, route.target) match {
      case (Some(rx), Some(t)) =>
        val regex = s"""$rx""".r
        val regex(strippedNum) = destination
        t.replace("\\1", strippedNum)
      case (Some(rx), None) =>
        val regex = s"""$rx""".r
        val regex(strippedNum) = destination
        strippedNum
      case (None, Some(t)) =>
        t.replace("\\1", destination)
      case (None, None) => destination
    }
  }

  def processTrunkInterfaces(channel: Channel, untransformedDstNum: String, interfaces: List[TrunkInterface], route: Route): List[ChannelVariablesSet] = {
    interfaces.zipWithIndex.flatMap {
      case (trunkInterface, index) =>
        val transformedDstNum = transformDestinationNumber(route, untransformedDstNum, trunkInterface.trunkLocation)
        trunkInterface.trunk match {
          case t: TrunkCustom =>
            setChannelVars(channel, s"$TRUNK_SUFFIX$index", Some(t.intfsuffix))
            setChannelVars(channel, s"$INTERFACE$index", Some(trunkInterface.interface))
            setChannelVars(channel, s"$TRUNK_EXTEN$index", Some(transformedDstNum))
            List(SetTrunkInterface(s"$INTERFACE$index"), SetTrunkExten(s"$TRUNK_EXTEN$index", transformedDstNum), SetTrunkSuffix(s"$TRUNK_SUFFIX$index"))
          case t =>
            setChannelVars(channel, s"$INTERFACE$index", Some(trunkInterface.interface))
            setChannelVars(channel, s"$TRUNK_EXTEN$index", Some(transformedDstNum))
            setChannelVars(channel, s"$TRUNK_GOTOMDS$index", Some(trunkInterface.trunkLocation.isOnAnotherMds.toString))
            List(SetTrunkInterface(s"$INTERFACE$index"), SetTrunkExten(s"$TRUNK_EXTEN$index", transformedDstNum), SetTrunkGotoMds(s"$TRUNK_GOTOMDS$index"))
        }
    }
  }

  private def transformDestinationNumber(route: Route, destination: String, trunkLocation: TrunkLocation): String = {
    trunkLocation match {
      case TrunkOnAnotherMds => destination
      case TrunkOnCurrentMds | TrunkUnknownMds => regexTransformation(route, destination)
    }
  }
}
