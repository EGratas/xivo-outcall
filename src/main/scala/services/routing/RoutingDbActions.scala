package services.routing

import anorm.SqlParser.{ get => getColumn }
import anorm._
import app.DatabaseConnectionPool
import models.{ Custom, Iax, Protocol, Sip }
import services.routing.RoutingDbActions._

import scala.concurrent.Future

object RoutingDbActions {
  case class MediaServer(id: Int, name: String, voip_ip: String)
  case class Route(id: Int, subroutine: Option[String], regexp: Option[String], target: Option[String], callerid: Option[String], internalCallerId: Boolean)
  case class RouteTrunk(routeId: Int, trunkfeatureId: Int, protocol: Protocol, protocolId: Int,
    mediaserverId: Int, mediaserverName: String, trunkLocation: TrunkLocation)
  case class TrunkInterface(trunk: Trunk, interface: String, suffix: Option[String], trunkLocation: TrunkLocation = TrunkOnCurrentMds)

  sealed trait Trunk
  case class TrunkSip(id: Int, name: String) extends Trunk
  case class TrunkIax(id: Int, name: String) extends Trunk
  case class TrunkCustom(id: Int, interface: String, intfsuffix: String) extends Trunk

  sealed trait TrunkLocation {
    def isOnAnotherMds: Boolean
  }
  case object TrunkOnCurrentMds extends TrunkLocation {
    def isOnAnotherMds = false
  }
  case object TrunkOnAnotherMds extends TrunkLocation {
    def isOnAnotherMds = true
  }
  case object TrunkUnknownMds extends TrunkLocation {
    def isOnAnotherMds = false
  }
}

class RoutingDbActions(db: DatabaseConnectionPool) {

  def getMediaserver(name: String): Future[Option[MediaServer]] = db.withConnection { implicit c =>
      val simple: RowParser[MediaServer] = Macro.namedParser[MediaServer]
      SQL(
        """
           SELECT
                       id, name, voip_ip
           FROM
                       mediaserver
           WHERE
                       name = {name}
        """.stripMargin
      )
        .on(Symbol("name") -> name)
        .as(simple.singleOpt)
  }

  def getRouteByPattern(destination: String, context: String, mediaServer: MediaServer): Future[List[Route]] =
    db.withConnection { implicit c =>
        val simple = getColumn[Int]("id") ~
          getColumn[Option[String]]("subroutine") ~
          getColumn[Option[String]]("regexp") ~
          getColumn[Option[String]]("target") ~
          getColumn[Option[String]]("callerid") ~
          getColumn[Boolean]("internal") map {
            case id ~ subroutine ~ regexp ~ target ~ callerid ~ internal =>
              Route(id, subroutine, regexp, target, callerid, internal)
          }
        SQL(
          """
          SELECT
                  route.id AS id, route.subroutine AS subroutine,
                  routepattern.regexp, routepattern.target, routepattern.callerid, route.internal
          FROM
                  route
          LEFT JOIN
                  routepattern ON route.id = routepattern.routeid
          LEFT JOIN
                  routecontext ON route.id = routecontext.routeid
          LEFT JOIN
                  routemediaserver ON route.id = routemediaserver.routeid
          WHERE
 	                {destination} SIMILAR TO routepattern.pattern
              AND
                  (routecontext.contextname = {context} OR routecontext.contextname IS NULL)
              AND
                  (routemediaserver.mdsid = {mdsId} OR routemediaserver.mdsid IS NULL)
          ORDER BY
                  route.priority ASC;
        """.stripMargin
        )
          .on(Symbol("destination") -> destination, Symbol("context") -> context, Symbol("mdsId") -> mediaServer.id)
          .as(simple.*)
    }

  def getRouteTrunk(routeId: Int): Future[List[RouteTrunk]] = db.withConnection { implicit c =>
      val simple = getColumn[Int]("routeId") ~
        getColumn[Int]("trunkfeatureId") ~
        getColumn[String]("protocol") ~
        getColumn[Int]("protocolId") ~
        getColumn[Int]("mediaserverId") ~
        getColumn[String]("mediaserverName") map {
          case tableRouteId ~ trunkfeatureId ~ protocol ~ protocolId ~ mediaserverId ~ mediaserverName =>
            val protocolType: Protocol = protocol match {
              case "sip" => Sip
              case "iax" => Iax
              case "custom" => Custom
              case _ => Custom
            }
            RouteTrunk(tableRouteId, trunkfeatureId, protocolType, protocolId, mediaserverId, mediaserverName, TrunkUnknownMds)
        }
      SQL(
        """
          SELECT
                  routetrunk.routeid AS routeId, trunkfeatures.id AS trunkfeatureId, trunkfeatures.protocol AS protocol,
                  trunkfeatures.protocolid AS protocolId, trunkfeatures.mediaserverid AS mediaserverId,
                  mediaserver.name AS mediaserverName
          FROM
                  trunkfeatures, routetrunk, mediaserver
          WHERE
 	                routetrunk.routeid = {routeId}
              AND
                  trunkfeatures.id = routetrunk.trunkid
              AND
                  trunkfeatures.mediaserverid = mediaserver.id
          ORDER BY
                  routetrunk.priority ASC;
        """.stripMargin
      )
        .on(Symbol("routeId") -> routeId)
        .as(simple.*)
  }

  def getSipTrunk(id: Int): Future[Option[TrunkSip]] = db.withConnection { implicit c =>
      val simple: RowParser[TrunkSip] = Macro.namedParser[TrunkSip]
      SQL(
        """
          SELECT
                  name, id
          FROM
                  usersip
          WHERE
                  id = {id}
              AND
                  category = 'trunk'
              AND
                  commented = 0
        """.stripMargin
      )
        .on(Symbol("id") -> id)
        .as(simple.singleOpt)
  }

  def getIaxTrunk(id: Int): Future[Option[TrunkIax]] = db.withConnection { implicit c =>
      val simple: RowParser[TrunkIax] = Macro.namedParser[TrunkIax]
      SQL(
        """
          SELECT
                  name, id
          FROM
                  useriax
          WHERE
                  id = {id}
              AND
                  category = 'trunk'
              AND
                  commented = 0
        """.stripMargin
      )
        .on(Symbol("id") -> id)
        .as(simple.singleOpt)
  }

  def getCustomTrunk(id: Int): Future[Option[TrunkCustom]] = db.withConnection { implicit c =>
      val simple: RowParser[TrunkCustom] = Macro.namedParser[TrunkCustom]
      SQL(
        """
          SELECT
                  id, interface, intfsuffix
          FROM
                  usercustom
          WHERE
                  id = {id}
              AND
                  category = 'trunk'
              AND
                  commented = 0
        """.stripMargin
      )
        .on(Symbol("id") -> id)
        .as(simple.singleOpt)
  }
}

