package ari.actions

import app.Configuration.ServiceId
import models.Channel

import scala.concurrent.Future

trait Actions[T] {
  def get(fsmId: ServiceId): Unit
  def get(id: String, fsmId: ServiceId): Future[Channel]
}
