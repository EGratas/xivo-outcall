package ari.actions

import app.Configuration.ServiceId
import ari.actions.Channels.HangupReason.HangupReason
import models.Channel
import models.events.bus._
import org.slf4j.{ Logger, LoggerFactory }
import services.AriEventBus
import services.AriEventBus._

import scala.concurrent.Future

object Channels {
  object HangupReason extends Enumeration {
    type HangupReason = Value
    val normal, busy, congestion, no_answer = Value
  }
}

class Channels(ariEventBus: AriEventBus, appName: OutcallApp) extends Actions[ChannelActions] {
  val log: Logger = LoggerFactory.getLogger(getClass)

  override def get(channelId: String, fsmId: ServiceId): Future[Channel] = ???

  def get(fsmId: ServiceId): Unit = {
    val getChannels = GetChannels
    publish(prepareEvent(getChannels, fsmId))
  }

  def getChannelVar(channelId: String, channelVariable: String, fsmId: ServiceId) = {
    val getChannelVar = GetChannelVar(channelId, channelVariable)
    publish(prepareEvent(getChannelVar, fsmId))
  }

  def setChannelVar(channelId: String, channelVariable: String, channelValue: Option[String], fsmId: ServiceId) = {
    val setChannelVar = SetChannelVar(channelId, ChannelVar(channelVariable, channelValue))
    publish(prepareEvent(setChannelVar, fsmId))
  }

  def continue(channelId: String, context: Option[String], extension: Option[String],
    priority: Option[Int], label: Option[String], fsmId: ServiceId): Unit = {
    val continue = ContinueInDialplan(channelId, context, extension, priority, label)
    publish(prepareEvent(continue, fsmId))
  }

  def hangup(channelId: String, reason: Option[HangupReason], fsmId: ServiceId) = {
    val hangupChannel = HangupChannel(channelId, reason)
    publish(prepareEvent(hangupChannel, fsmId))
  }

  private def prepareEvent(action: BusEvents, fsmId: ServiceId): AriEvent = {
    AriEvent(AriTopic(TopicType.RESTREQUEST, DefaultApp), AriRequestMessage(action, appName, fsmId))
  }

  private def publish(event: AriEvent) = ariEventBus.publish(event)
}
