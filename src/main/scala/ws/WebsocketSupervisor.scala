package ws

import akka.actor.{ OneForOneStrategy, Props, SupervisorStrategy }
import akka.pattern.{ BackoffOpts, BackoffSupervisor }
import app.Configuration
import org.slf4j.{ Logger, LoggerFactory }

object WebsocketSupervisor {
  val log: Logger = LoggerFactory.getLogger(getClass)

  def initWebsocketActor(config: Configuration, childProps: Props): Props = {

    val backoffOptions = BackoffOpts.onFailure(
      childProps,
      childName = "websocket",
      minBackoff = config.restartRetryMin,
      maxBackoff = config.restartRetryMax,
      randomFactor = 0.2
    ).withMaxNrOfRetries(-1)
      .withSupervisorStrategy(OneForOneStrategy(loggingEnabled = false) {
        case e: Exception =>
          log.error(s"Websocket is being restarted due to ${e.getMessage}")
          SupervisorStrategy.Restart
      })
    BackoffSupervisor.props(backoffOptions)
  }
}
