package ws

import akka.Done
import akka.actor.{ Actor, ActorSystem, Cancellable, Props }
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.ws._
import akka.stream.scaladsl._
import app.Configuration
import models.events.ws.{ UnsupportedEvent, WsEvents }
import org.slf4j.{ Logger, LoggerFactory }
import play.api.libs.json.{ JsError, JsSuccess, Json }
import services.AriEventBus
import services.AriEventBus._

import scala.concurrent.duration._
import scala.concurrent.{ Future, Promise }
import scala.util.{ Failure, Success }

object WebsocketAsSink {
  val log: Logger = LoggerFactory.getLogger(getClass)

  case object ConnectionClosed {
    override def toString: String = "connection closed"
  }
  case object Connected
  case object ConnectionTimeout
  case object CloseWebsocket
  def websocketProps(config: Configuration, ariEventBus: AriEventBus, websocketRequestFactory: WebsocketRequestFactory): Props = {
    Props(new WebsocketAsSink(config, ariEventBus, websocketRequestFactory))
  }
}

class WebsocketAsSink(config: Configuration, ariEventBus: AriEventBus, singleWSRequestFactory: WebsocketRequestFactory) extends Actor {
  import WebsocketAsSink._
  implicit val system: ActorSystem = context.system
  import system.dispatcher

  val log: Logger = LoggerFactory.getLogger(getClass)
  val outcallServices: List[OutcallApp] = List(ScheduleApp, RightsApp, RoutingApp, InitChannelVarsApp)

  private def initWebsocket: List[(Future[Done], (Future[Done], Promise[Option[Message]]))] = {
    val wsRequests = outcallServices.map(app => (app, singleWSRequestFactory.getWsRequest(app, websocketFlow)))

    val connectedClosedList: List[(Future[Done], (Future[Done], Promise[Option[Message]]))] = wsRequests.map { upgradedResponse =>
      val whenClosed: (Future[Done], Promise[Option[Message]]) = upgradedResponse._2._2
      val whenConnected: Future[Done] = connected(upgradedResponse._2._1)

      whenConnected.onComplete {
        case Success(_) => log.info(s"Connecting application: ${upgradedResponse._1} to ${config.ariHost}:${config.ariPort}")
        case Failure(exception) => throw new Exception(exception)
      }

      whenClosed._1.onComplete(_ => log.info(s"Closing application ${upgradedResponse._1}"))

      (whenConnected, whenClosed)
    }

    connectedClosedList
  }

  private def connected(upgradeResponse: Future[WebSocketUpgradeResponse]): Future[Done] =
    upgradeResponse.map { upgrade =>
      if (upgrade.response.status == StatusCodes.SwitchingProtocols) {
        Done
      } else {
        throw new RuntimeException(s"Connection failed: ${upgrade.response.status}")
      }
    }

  override def preStart(): Unit = {
    log.info(s"$self starting")
    val wsConnectionAsList = initWebsocket
    wsConnectionAsList.foreach { cc =>
      cc._1.map(result => self ! WebsocketAsSink.Connected)
      cc._2._1.map(_ => self ! ConnectionClosed)
    }
    val cnxTimeoutSchedule = context.system.scheduler.scheduleOnce(2.seconds, self, WebsocketAsSink.ConnectionTimeout)
    context.become(cancelTimeout(cnxTimeoutSchedule).orElse(receive))
  }

  override def postStop(): Unit = {
    log.info(s"$self stop")
  }

  def cancelTimeout(cnxTimeoutSchedule: Cancellable): Receive = {
    case Connected =>
      cnxTimeoutSchedule.cancel()
      log.info(s"Websocket connected")
      context.become(receive)
  }

  override def receive = {
    case ConnectionTimeout => self ! ConnectionClosed
    case ConnectionClosed => throw new Exception(ConnectionClosed.toString)
    case _ =>
  }

  private def createAriEvent(wsEvent: WsEvents): AriEvent = {
    val appName = wsEvent.recevingApplication match {
      case "schedules" => ScheduleApp
      case "rights" => RightsApp
      case "routing" => RoutingApp
      case "initchannelvars" => InitChannelVarsApp
      case _ => DefaultApp
    }
    AriEvent(AriTopic(TopicType.WSEVENT, appName), AriWsMessage(wsEvent))
  }

  def processEvents(message: TextMessage.Strict): Option[WsEvents] = {
    Json.parse(message.text).validate[WsEvents] match {
      case e: JsError =>
        log.error(s"Non understandable ws event received: $e - ${message.text}")
        throw new Exception("Non understandable ws event received")
      case message: JsSuccess[WsEvents] =>
        message.value match {
          case u: UnsupportedEvent =>
            log.debug(s"Unsupported event received: $u")
            None
          case wsEvent: WsEvents =>
            log.debug(s"Converted to ws event $wsEvent")
            Some(wsEvent)
        }
    }
  }

  val incomingMessage: Sink[Message, Future[Done]] =
    Sink.foreach {
      case message: TextMessage.Strict =>
        log.debug(s"Received ws message ${message.text}")
        processEvents(message).foreach(e => ariEventBus.publish(createAriEvent(e)))
      case other => log.debug(s"$other")
    }

  val outgoingMessage: Source[Message, Promise[Option[Message]]] = Source.maybe[Message]

  val websocketFlow: Flow[Message, Message, (Future[Done], Promise[Option[Message]])] =
    Flow.fromSinkAndSourceMat(incomingMessage, outgoingMessage)(Keep.both)
}
