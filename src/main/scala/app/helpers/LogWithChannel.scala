package app.helpers

import models.Channel
import org.slf4j.{ Logger, LoggerFactory }

trait LogWithChannel {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  private def logMessage(channel: Channel, msg: String): String = s"Channel ${channel.name} (${channel.id}) : $msg"

  object chanLog {
    def debug(channel: Channel, msg: String): Unit = logger.debug(logMessage(channel, msg))
    def info(channel: Channel, msg: String): Unit = logger.info(logMessage(channel, msg))
    def error(channel: Channel, msg: String): Unit = logger.error(logMessage(channel, msg))
    def warn(channel: Channel, msg: String): Unit = logger.warn(logMessage(channel, msg))
  }
}
