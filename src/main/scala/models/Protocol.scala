package models

import services.routing.RoutingDbActions._

sealed trait Protocol

case object Sip extends Protocol {
  def interface(trunk: TrunkSip): TrunkInterface = TrunkInterface(trunk, s"SIP/${trunk.name}", None, TrunkOnCurrentMds)
}

case object Iax extends Protocol {
  def interface(trunk: TrunkIax): TrunkInterface = TrunkInterface(trunk, s"IAX/${trunk.name}", None, TrunkOnCurrentMds)
}

case object Mds extends Protocol {
  def interface(trunk: TrunkSip): TrunkInterface = TrunkInterface(trunk, s"SIP/to-${trunk.name}", None, TrunkOnAnotherMds)
}

case object Custom extends Protocol {
  def interface(trunk: TrunkCustom): TrunkInterface = TrunkInterface(trunk, trunk.interface, Some(trunk.intfsuffix), TrunkOnCurrentMds)
}
