package models.events.ws

import models.Channel
import play.api.libs.json._

case class StasisStart(args: List[String], channel: Channel, asterisk_id: String, application: String) extends WsEvents {
  override val recevingApplication: String = application
}

object StasisStart {
  implicit val stasisStartReads: Reads[StasisStart] = Json.reads[StasisStart]
}
