package models.events.ws

import models.Channel
import org.slf4j.{ Logger, LoggerFactory }
import play.api.libs.functional.syntax._
import play.api.libs.json._

trait WsEvents {
  val recevingApplication: String
}

trait ChannelWsEvents extends WsEvents {
  val channel: Channel
}

object WsEvents {
  val log: Logger = LoggerFactory.getLogger(getClass)
  implicit val eventsReads: Reads[WsEvents] = Reads[WsEvents] { json =>
    (json \ "type").asOpt[String] match {
      case Some(_) =>
        (json \ "type").validate[String].flatMap {
          case "StasisStart" => json.validate[StasisStart]
          case "StasisEnd" => json.validate[StasisEnd]
          case "ChannelVarset" => json.validate[ChannelVarset]
          case _ => json.validate[UnsupportedEvent]
        }
      case None =>
        JsError("Event type missing")
    }
  }
}

case class UnsupportedEvent(eventType: String, application: String) extends WsEvents {
  override val recevingApplication: String = application
}

object UnsupportedEvent {
  implicit val unsupportedEventReads: Reads[UnsupportedEvent] =
    ((JsPath \ "type").read[String] and
      (JsPath \ "application").read[String])(UnsupportedEvent.apply _)
}
