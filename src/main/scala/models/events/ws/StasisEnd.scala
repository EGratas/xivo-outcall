package models.events.ws

import models.Channel
import play.api.libs.json._

case class StasisEnd(channel: Channel, asterisk_id: String, application: String) extends WsEvents {
  override val recevingApplication: String = application
}

object StasisEnd {
  implicit val stasisEndReads: Reads[StasisEnd] = Json.reads[StasisEnd]
}
