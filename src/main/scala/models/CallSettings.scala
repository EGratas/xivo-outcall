package models

import services.rights.RightsDbActions.RightCall

sealed trait CallSettings {
  val variableName: String
  val variableValue: String
}

case class CallAuthorization(variableName: String, variableValue: String) extends CallSettings
case class CallPassword(variableName: String, variableValue: String) extends CallSettings

sealed trait ForwardType
case object ForwardXivo extends ForwardType
case object ForwardSip extends ForwardType

sealed trait Forwarded
case class ForwardedUser(id: String) extends Forwarded
case object ForwardedElseWhere extends Forwarded

sealed trait CallRights
case class RightsFound(ids: List[RightCall]) extends CallRights
case object RightsNotFound extends CallRights
case class RightsFailed(f: Throwable) extends CallRights
