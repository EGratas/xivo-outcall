package models

import services.rights.RightsDbActions.RightCallExten
import services.rights.RightsFsm.RightsFsmContextWitUserId
import services.routing.RoutingDbActions.{ Route, RouteTrunk }

import scala.concurrent.Future

sealed trait DatabaseQuery

sealed trait InitChannelVarsQuery extends DatabaseQuery
case class GetForwardedUserId(action: Future[Forwarded]) extends InitChannelVarsQuery

sealed trait RightsDbQuery extends DatabaseQuery
case class GetRightsIds(callId: RightsFsmContextWitUserId) extends RightsDbQuery
case class GetRightCallUser(id: String, extenIds: List[RightCallExten]) extends RightsDbQuery
case class GetUser(id: Int) extends RightsDbQuery
case class GetGroupRights(userId: Int, rightCallIds: List[RightCallExten])
case class GetRightCallRoute(routeId: String, rightCallsIds: List[RightCallExten])

sealed trait RoutingDbQuery extends DatabaseQuery
case class GetTrunkInterface(routeTrunks: List[RouteTrunk], route: Route) extends RoutingDbQuery
case class GetRoute(channelData: ChannelData) extends RoutingDbQuery
case class GetRouteTrunk(routes: List[Route]) extends RoutingDbQuery
